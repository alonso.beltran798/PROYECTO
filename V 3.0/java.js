$(document).ready(function() {
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botón Cambio de Tema-------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $('#white').click(function(event) {
        $(":root").get(0).style.setProperty("--color-dark", "white");
        $(":root").get(0).style.setProperty("--color-light", "#2b2b2b");
        $("#white").css("display", "none");
        $("#black").css("display", "block");
    });
    $('#black').click(function(event) {
        $(":root").get(0).style.setProperty("--color-dark", "#2b2b2b");
        $(":root").get(0).style.setProperty("--color-light", "white");
        $("#white").css("display", "block");
        $("#black").css("display", "none");
    });
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botón Scroll hacia arriba--------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $('.ir-arriba').click(function(){
		$('body, html').animate({
			scrollTop: '0px'
		}, 300);
	});
	$(window).scroll(function(){
		if( $(this).scrollTop() > 500 ){
			$('.ir-arriba').slideDown(300);
		} else {
			$('.ir-arriba').slideUp(300);
		}
	});
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes de Formularios-----------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $('.envio').click(function(event) {
        if ($('input[name$="Usuario"]').text == '111') {
            $('.entrar').fadeOut(500);
            $(".crear").delay(500).fadeIn(500);
        }
    });
    $('.but').click(function(event) {
        if ($('.entrar').css("display") == "block") {
            $('.entrar').fadeOut(500);
            $(".crear").delay(500).fadeIn(500);
            $('html,body').animate({
                scrollTop: $(".entrar").offset().top
            }, 1000);
        }else{
            $('.crear').fadeOut(500);
            $(".entrar").delay(500).fadeIn(500);
            $('html,body').animate({
                scrollTop: $(".crear").offset().top
            }, 1000);
        }
    });
    $('.closeform').click(function(event) {
        $("#todo").animate({opacity:1});
        if ($('.entrar').css("display") == "block") {
            $('.entrar').fadeOut(200);
        }else if($('.productaside').css("display") == "block") {
            $('.productaside').fadeOut(200);
        }else{
            $('.crear').fadeOut(200);
        }
        $('html, body').css("overflow", "auto");
        $('.content').css("z-index", "-1");
    });
    $('.openform').click(function(event) {
        if ($('.productaside').css("display") == "block") {
            $('.productaside').fadeOut(200);
            $('.entrar').delay(200).fadeIn(500);
        }else{
            $("#todo").animate({opacity:0.4});
            $('.entrar').fadeIn(500);
            $('.content').css("z-index", "999");
            $('html,body').animate({
                scrollTop: $(".entrar").offset().top
            }, 1000);
        }
    });
    $('.openformpass').click(function(event) {
        if ($('.productaside').css("display") == "block") {
            $('.productaside').fadeOut(200);
            $('.entrar').delay(200).fadeIn(500);
        }else{
            $("#todo").animate({opacity:0.4});
            $('.crear').fadeIn(500);
            $('.content').css("z-index", "999");
            $('html,body').animate({
                scrollTop: $(".crear").offset().top
            }, 1000);
        }
    });
    $('.butproduct').click(function(event) {
        $("#todo").animate({opacity:0.4});
        $('.content').css("z-index", "999");
        if ($('.productaside').css("display") == "block") {
            $('.productaside').fadeOut(200);
        }else{
            $('.productaside').fadeIn(500);
        }
    });
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botón Menú-----------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $('.butmenu').click(function(event) {
        if ($("#menudesplegable").css("display") == "none") {
            $("#menudesplegable").fadeIn(100);
            $("#menudesplegable").css("height", "100px");
            $('.butmenuabierto').css("height", "90%");
            $('.changeimg').fadeOut(100);
            $('.changeimg2').delay(100).fadeIn(100);
        }else{
            $("#menudesplegable").css("height", "0");
            $('.butmenuabierto').css("height", "0");
            $("#menudesplegable").fadeOut(500);
            $('.changeimg2').fadeOut(100);
            $('.changeimg').delay(100).fadeIn(100);
        }
    });
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes Scroll Pantalla----------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $(".conocenos").click(function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $("#conocenos").offset().top
        }, 2000);
    });
    $(".productosmen").click(function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $("#productosmen").offset().top
        }, 2000);
    });
    $(".contactanos").click(function (e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: $("#contactanos").offset().top
        }, 2000);
    });
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes de Cuenta----------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $("#name").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#name").css("background-color", "darkgreen");
        $("#name .change").css("display", "block");
        $("#name .change").css("height", "80");
        $("#name .edit").css("display", "none");
    });
    $("#lastname").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#lastname").css("background-color", "darkgreen");
        $("#lastname .change").css("display", "block");
        $("#lastname .change").css("height", "80");
        $("#lastname .edit").css("display", "none");
    });
    $("#birthdaydate").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#birthdaydate").css("background-color", "darkgreen");
        $("#birthdaydate .change").css("display", "block");
        $("#birthdaydate .change").css("height", "80");
        $("#birthdaydate .edit").css("display", "none");
    });
    $("#identification").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#identification").css("background-color", "darkgreen");
        $("#identification .change").css("display", "block");
        $("#identification .change").css("height", "80");
        $("#identification .edit").css("display", "none");
    });
    $("#telephone").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#telephone").css("background-color", "darkgreen");
        $("#telephone .change").css("display", "block");
        $("#telephone .change").css("height", "80");
        $("#telephone .edit").css("display", "none");
    });
    $("#email").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#email").css("background-color", "darkgreen");
        $("#email .change").css("display", "block");
        $("#email .change").css("height", "80");
        $("#email .edit").css("display", "none");
    });
    $("#username").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#username").css("background-color", "darkgreen");
        $("#username .change").css("display", "block");
        $("#username .change").css("height", "80");
        $("#username .edit").css("display", "none");
    });
    $("#pass").click(function (e) {
        $(".change").css("display", "none");
        $(".change").css("height", "0");
        $(".edit").css("display", "block");
        $(".infouser").css("background-color", "");
        $("#pass").css("background-color", "darkgreen");
        $("#pass .change").css("display", "block");
        $("#pass .change").css("height", "80");
        $("#pass .edit").css("display", "none");
    });
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes de Productos-------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
    $("#poliestireno").click(function(event) {
        $('.h1product').html('Poliestireno');
        $('.imgproduct').attr("src", "img/polestireno.jpg");
        $('.precio').html(' $2500');
        $('.cantidad').html(' 45kg');
    });
    $("#carton").click(function(event) {
        $('.h1product').html('Cartón');
        $('.imgproduct').attr("src", "img/carton.jpg");
        $('.precio').html(' $1000');
        $('.cantidad').html(' 250kg');
    });
    $("#tubopvc").click(function(event) {
        $('.h1product').html('Tubo de Pvc');
        $('.imgproduct').attr("src", "img/pvc.jpg");
        $('.precio').html(' $1600');
        $('.cantidad').html(' 45kg');
    });
    $("#archivo").click(function(event) {
        $('.h1product').html('Archivo');
        $('.imgproduct').attr("src", "img/archivo.jpg");
        $('.precio').html(' $1000');
        $('.cantidad').html(' 250kg');
    });
    $("#plegadiza").click(function(event) {
        $('.h1product').html('Plegadiza');
        $('.imgproduct').attr("src", "img/pleagadiza.jpg");
        $('.precio').html(' $1600');
        $('.cantidad').html(' 45kg');
    });
    $("#polietileno").click(function(event) {
        $('.h1product').html('Polietileno');
        $('.imgproduct').attr("src", "img/poli.jpg");
        $('.precio').html(' $2500');
        $('.cantidad').html(' 25kg');
    });
    $("#hierro").click(function(event) {
        $('.h1product').html('Hierro');
        $('.imgproduct').attr("src", "img/hierro.jpg");
        $('.precio').html(' $3100');
        $('.cantidad').html(' 70kg');
    });
    $("#aluminio").click(function(event) {
        $('.h1product').html('Aluminio');
        $('.imgproduct').attr("src", "img/alumini.jpg");
        $('.precio').html(' $2000');
        $('.cantidad').html(' 46kg');
    });
    $("#costal").click(function(event) {
        $('.h1product').html('Costal');
        $('.imgproduct').attr("src", "img/costales.jpg");
        $('.precio').html(' $3600');
        $('.cantidad').html(' 10kg');
    });
    $("#cobre").click(function(event) {
        $('.h1product').html('Cobre');
        $('.imgproduct').attr("src", "img/COBRE.jpg");
        $('.precio').html(' $17000');
        $('.cantidad').html(' 15kg');
    });
});
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes de Campos Cuenta---------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
function newname(){
    var porClassName=document.getElementById("inputname").value;
    document.getElementById("iname").innerHTML=porClassName;
    document.getElementById("inputname").value = "";
}
function newlastname(){
    var porClassName=document.getElementById("inputlastname").value;
    document.getElementById("ilastname").innerHTML=porClassName;
    document.getElementById("inputlastname").value = "";
}
function newidentification(){
    var porClassName=document.getElementById("inputidentification").value;
    document.getElementById("iidentification").innerHTML=porClassName;
    document.getElementById("inputidentification").value = "";
}
function newtelephone(){
    var porClassName=document.getElementById("inputtelephone").value;
    document.getElementById("itelephone").innerHTML=porClassName;
    document.getElementById("inputtelephone").value = "";
}
function newemail(){
    var porClassName=document.getElementById("inputemail").value;
    document.getElementById("iemail").innerHTML=porClassName;
    document.getElementById("inputemail").value = "";
}
function newusername(){
    var porClassName=document.getElementById("inputusername").value;
    document.getElementById("iusername").innerHTML=porClassName;
    document.getElementById("inputusername").value = "";
}
function newpass(){
    var porClassName=document.getElementById("inputpass").value;
    document.getElementById("ipass").innerHTML=porClassName;
    document.getElementById("inputpass").value = "";
}
function showpass(){
    pass=document.getElementById("inputpass");
    show=document.getElementById("show");
    eye=document.getElementById("eye");
    if(pass.type=="password"){
        pass.type = "text";
        eye.title = "Ocultar Contraseña";
        show.style.display = 'none';
    }else{
        pass.type = "password";
        eye.title = "Mostrar Contraseña";
        show.style.display = 'block';
    }
}
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes de Barra de Busqueda-----------------------------------------------
//------------------------------------------------------------------------------------------------------------------
document.onkeyup = enter;
function enter(event){
    var search = document.getElementById('busqueda').value.toLowerCase();
    var tecla =  event.keyCode ;
    if(tecla==13)
    {
       doSearch();
    }
    if(search === '')
    {
       doSearch();
    }
}
function doSearch(){
    var tableReg = document.getElementById('datos');
    var searchText = document.getElementById('busqueda').value.toLowerCase();
    var cellsOfRow="";
    var found=false;
    var compareWith="";
    for (var i = 1; i < tableReg.rows.length; i++){
        cellsOfRow = tableReg.rows[i].getElementsByTagName('td');
        found = false;
        for (var j = 0; j < cellsOfRow.length && !found; j++)
        {
            compareWith = cellsOfRow[j].innerHTML.toLowerCase();
            if (searchText.length == 0 || (compareWith.indexOf(searchText) > -1))
            {
                found = true;
            }
        }
        if(found)
        {
            tableReg.rows[i].style.display = '';
        } else {
            tableReg.rows[i].style.display = 'none';
        }
    }
}
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Label de Fecha-------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
var date = new Date();
var d  = date.getDate();
var day = (d < 10) ? '0' + d : d;
var m = date.getMonth() + 1;
var month = (m < 10) ? '0' + m : m;
var yy = date.getYear();
var year = (yy < 1000) ? yy + 1900 : yy;