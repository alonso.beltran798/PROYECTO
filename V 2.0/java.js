$(document).ready(function() {
                    $('.but').click(function(event) {
                        if ($('.entrar').css("display") == "block") {
                            $('.entrar').fadeOut(500);
                            $(".crear").delay(500).fadeIn(500);
                        }else{
                            $('.crear').fadeOut(500);
                            $(".entrar").delay(500).fadeIn(500);
                        }
                    });
                    $('.closeform').click(function(event) {
                        $("#todo").animate({opacity:1});
                        if ($('.entrar').css("display") == "block") {
                            $('.entrar').fadeOut(200);
                        }else if($('.productaside').css("display") == "block") {
                            $('.productaside').fadeOut(200);
                        }else{
                            $('.crear').fadeOut(200);
                        }
                        $('html, body').css("overflow", "auto");
                        $('.content').css("z-index", "0");
                    });
                    $('.openform').click(function(event) {
                        if ($('.productaside').css("display") == "block") {
                            $('.productaside').fadeOut(200);
                            $('.entrar').delay(200).fadeIn(500);
                        }else{
                            $("#todo").animate({opacity:0.4});
                            $('.entrar').fadeIn(500);
                            $('html, body').css("overflow", "hidden");
                            $('.content').css("z-index", "999");
                        }
                    });
                    $('.openformpass').click(function(event) {
                        if ($('.productaside').css("display") == "block") {
                            $('.productaside').fadeOut(200);
                            $('.entrar').delay(200).fadeIn(500);
                        }else{
                            $("#todo").animate({opacity:0.4});
                            $('.crear').fadeIn(500);
                            $('html, body').css("overflow", "hidden");
                            $('.content').css("z-index", "999");
                        }
                    });
                    $('.butproduct').click(function(event) {
                        $("#todo").animate({opacity:0.4});
                        $('html, body').css("overflow", "hidden");
                        $('.content').css("z-index", "999");
                        if ($('.productaside').css("display") == "block") {
                            $('.productaside').fadeOut(200);
                        }else{
                            $('.productaside').fadeIn(500);
                        }
                    });
                    $('.butmenu').click(function(event) {
                        if ($("#menudesplegable").css("display") == "none") {
                            $("#menudesplegable").fadeIn(500);
                            $("#menudesplegable").css("height", "100px");
                            $('.butmenuabierto').css("height", "90%");
                            $('.changeimg').fadeOut(100);
                            $('.changeimg2').delay(100).fadeIn(100);
                        }else{
                            $("#menudesplegable").css("height", "0");
                            $('.butmenuabierto').css("height", "0");
                            $("#menudesplegable").fadeOut(500);
                            $('.changeimg2').fadeOut(100);
                            $('.changeimg').delay(100).fadeIn(100);
                        }
                    });
//------------------------------------------------------------------------------------------------------------------
//---------------------------------------Botónes de Productos-------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
                    $("#poliestireno").click(function(event) {
                        $('.h1product').html('Poliestireno');
                        $('.imgproduct').attr("src", "img/polestireno.jpg");
                        $('.precio').html(' $2500');
                        $('.cantidad').html(' 45kg');
                    });
                    $("#carton").click(function(event) {
                        $('.h1product').html('Cartón');
                        $('.imgproduct').attr("src", "img/carton.jpg");
                        $('.precio').html(' $1000');
                        $('.cantidad').html(' 250kg');
                    });
                    $("#tubopvc").click(function(event) {
                        $('.h1product').html('Tubo de Pvc');
                        $('.imgproduct').attr("src", "img/pvc.jpg");
                        $('.precio').html(' $1600');
                        $('.cantidad').html(' 45kg');
                    });
                    $("#archivo").click(function(event) {
                        $('.h1product').html('Archivo');
                        $('.imgproduct').attr("src", "img/archivo.jpg");
                        $('.precio').html(' $1000');
                        $('.cantidad').html(' 250kg');
                    });
                    $("#plegadiza").click(function(event) {
                        $('.h1product').html('Plegadiza');
                        $('.imgproduct').attr("src", "img/pleagadiza.jpg");
                        $('.precio').html(' $1600');
                        $('.cantidad').html(' 45kg');
                    });
                    $("#polietileno").click(function(event) {
                        $('.h1product').html('Polietileno');
                        $('.imgproduct').attr("src", "img/poli.jpg");
                        $('.precio').html(' $2500');
                        $('.cantidad').html(' 25kg');
                    });
                    $("#hierro").click(function(event) {
                        $('.h1product').html('Hierro');
                        $('.imgproduct').attr("src", "img/hierro.jpg");
                        $('.precio').html(' $3100');
                        $('.cantidad').html(' 70kg');
                    });
                    $("#aluminio").click(function(event) {
                        $('.h1product').html('Aluminio');
                        $('.imgproduct').attr("src", "img/alumini.jpg");
                        $('.precio').html(' $2000');
                        $('.cantidad').html(' 46kg');
                    });
                    $("#costal").click(function(event) {
                        $('.h1product').html('Costal');
                        $('.imgproduct').attr("src", "img/costales.jpg");
                        $('.precio').html(' $3600');
                        $('.cantidad').html(' 10kg');
                    });
                    $("#cobre").click(function(event) {
                        $('.h1product').html('Cobre');
                        $('.imgproduct').attr("src", "img/COBRE.jpg");
                        $('.precio').html(' $17000');
                        $('.cantidad').html(' 15kg');
                    });
                });